// CONFIG
// const DELETE_ZIPS = true; //true or false
// const ORIGINAL_ROM_FILE_PATH = "PathToMyRom.sfc" // PATH TO ROM
// const PATCHES_FOLDER = "/My/Patches/Path" // PATH TO ALL YOUR BPS PATCHES (supports subfolder and zips)
// const SAVE_TO_FOLDER = "/Save/Roms/here" // WHERE TO SAVE YOUR ROMS


var ORIGINAL_ROM_FILE_PATH = "/Users/gilles/Desktop/Mario Hacks/Orginal/Super Mario World (USA).sfc" // PATH TO ROM
var PATCHES_FOLDER = "/Users/gilles/Desktop/Mario Hacks/New Patches" // PATH TO ALL YOUR BPS PATCHES (supports subfolder and zips)
var SAVE_TO_FOLDER = "/Users/gilles/Desktop/Mario Hacks/Patched Games New" // WHERE TO SAVE YOUR ROMS
var DELETE_ZIPS = false; //true or false
var externalConsole = null;

/*
 *  DO NOT EDIT BELOW THIS LINE UNLESS YOU KNOW WHAT YOU DO!
 */

const fs = require("fs");
const path = require('path');
const unzipper = require("unzipper");

function setConfig(romfile, patchpath, savePath, evt=null) {
    ORIGINAL_ROM_FILE_PATH = romfile;
    PATCHES_FOLDER = patchpath;
    SAVE_TO_FOLDER = savePath;
    externalConsole = evt ? (msg) => {evt.reply("log", msg)} : null;
}

function log(...msg) {
  if(externalConsole) {
      externalConsole(msg);
  } else {
      console.log(msg);
  }
}

async function runFiles() {
  ensureDirectoryExistence(SAVE_TO_FOLDER);
  const allZips = getAllFiles(PATCHES_FOLDER).filter((filepath) => filepath.endsWith(".zip"));
    for(const zipPath of allZips) {
       await unzipFile(zipPath);
    }

  const allPatches = getAllFiles(PATCHES_FOLDER).filter((filepath) => filepath.endsWith(".bps"));
  if(allPatches.length > 0) {
      log("Write Patched Games to "+SAVE_TO_FOLDER);
  }  else {
      log("No patches found in folder "+PATCHES_FOLDER);
  }
  for(const patchPath of allPatches) {
      tryPatch(ORIGINAL_ROM_FILE_PATH, patchPath);
  }
}

async function unzipFile(filepath) {
    var justPath = path.basename(filepath, ".zip");
    var dirname = path.dirname(filepath);
    var list = [];
    const zip = fs.createReadStream(filepath).pipe(unzipper.Parse({forceStream: true}));
    for await (const entry of zip) {
        const fileName = entry.path;
        const ext = getExt(fileName);
        if (fileName.endsWith("bps")) {
            const fullFileName = dirname + "/" + justPath + "/" + justPath+ext;
            fs.mkdirSync(path.dirname(fullFileName), { recursive: true });
            entry.pipe(fs.createWriteStream(fullFileName));
            list.push(fullFileName);
        } else {
            entry.autodrain();
        }
    }

    //console.log("All files in zip done", filepath);
    if(DELETE_ZIPS) {
        log("Delete zip now!", filepath);
        fs.rmSync(filepath);
    }
    return list;
}

function getExt(filename) {
    var fileEndings = ["bps"];
    for(var fileExt of fileEndings) {
        if(filename.endsWith("."+fileExt)) {
            return "."+fileExt
        }
    }
    return null;
}

const getAllFiles = function(dirPath, arrayOfFiles = []) {
    const files = fs.readdirSync(dirPath)

    arrayOfFiles = arrayOfFiles || []

    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
        } else {
            arrayOfFiles.push(path.join(dirPath, "/", file))
        }
    })

    return arrayOfFiles
}





function tryPatch(originalFilePath, patchFilePath)
{
    //log("Try to patch", patchFilePath);
    var romname = originalFilePath;
    var romfile = fs.readFileSync(romname);
    var romdata = {bytes: romfile, name: path.basename(romname), mime: ''};

    var bpsname = patchFilePath;
    var bpsfile = fs.readFileSync(bpsname);
    var bpsdata = {bytes: bpsfile, name: path.basename(bpsname), mime: ''};

    handleBps(romdata, bpsdata);
}

function save(data, filename, mime) {
  //log("Save" , filename);
    try {
        fs.writeFileSync(SAVE_TO_FOLDER+"/"+filename, data);
        log("Saved "+filename);
    }
    catch (e) {
        log("Already exists: "+filename, e);
    }
}

//no error checking, other than BPS signature, input size/crc and JS auto checking array bounds
function applyBps(rom, patch)
{
    function crc32(bytes) {
        var c;
        var crcTable = [];
        for(var n =0; n < 256; n++){
            c = n;
            for(var k =0; k < 8; k++){
                c = ((c&1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
            }
            crcTable[n] = c;
        }

        var crc = 0 ^ (-1);
        for (var i = 0; i < bytes.length; i++ ) {
            crc = (crc >>> 8) ^ crcTable[(crc ^ bytes[i]) & 0xFF];
        }
        return (crc ^ (-1)) >>> 0;
    };

    var patchpos = 0;
    function u8() { return patch[patchpos++]; }
    function u32at(pos) { return (patch[pos+0]<<0 | patch[pos+1]<<8 | patch[pos+2]<<16 | patch[pos+3]<<24)>>>0; }

    function decode()
    {
        var ret = 0;
        var sh = 0;
        while (true)
        {
            var next = u8();
            ret += (next^0x80) << sh;
            if (next&0x80) return ret;
            sh += 7;
        }
    }

    function decodes()
    {
        var enc = decode();
        var ret = enc>>1;
        if (enc&1) ret=-ret;
        return ret;
    }

    if (u8()!=0x42 || u8()!=0x50 || u8()!=0x53 || u8()!=0x31) throw "not a BPS patch";
    if (decode() != rom.length) throw "wrong input file";
    if (crc32(rom) != u32at(patch.length-12)) throw "wrong input file";

    var out = new Uint8Array(decode());
    var outpos = 0;

    var metalen = decode();
    patchpos += metalen; // can't join these two, JS reads patchpos before calling decode

    var SourceRead=0;
    var TargetRead=1;
    var SourceCopy=2;
    var TargetCopy=3;

    var inreadpos = 0;
    var outreadpos = 0;

    while (patchpos < patch.length-12)
    {
        var thisinstr = decode();
        var len = (thisinstr>>2)+1;
        var action = (thisinstr&3);

        switch (action)
        {
            case SourceRead:
            {
                for (var i=0;i<len;i++)
                {
                    out[outpos] = rom[outpos];
                    outpos++;
                }
            }
                break;
            case TargetRead:
            {
                for (var i=0;i<len;i++)
                {
                    out[outpos++] = u8();
                }
            }
                break;
            case SourceCopy:
            {
                inreadpos += decodes();
                for (var i=0;i<len;i++) out[outpos++] = rom[inreadpos++];
            }
                break;
            case TargetCopy:
            {
                outreadpos += decodes();
                for (var i=0;i<len;i++) out[outpos++] = out[outreadpos++];
            }
                break;
        }
    }

    return out;
}


function handleBps(rom, patch)
{
    try {
        var ret;
        try {
            ret = applyBps(new Uint8Array(rom.bytes), new Uint8Array(patch.bytes));
        } catch(e) {
            if (e === "wrong input file") {
                // maybe a headered rom? skip first 512 bytes for patching
                ret = applyBps(new Uint8Array(rom.bytes, 512), new Uint8Array(patch.bytes));
                // if we reached here, there were no errors, so the assumption about a headered rom was correct.
                // now re-add the 512 bytes from the original ROM to the patched one
                var tmpbuf = new Uint8Array(ret.length + 512); // create buffer large enough for rom and header
                tmpbuf.set(new Uint8Array(rom.bytes, 512)); // copy header
                tmpbuf.set(ret, 512); // copy rom data
                ret = tmpbuf;
            }
            else throw e;
        }
        var basename = patch.name.substring(0, patch.name.lastIndexOf("."));
        var ext = '.'+rom.name.split(".").pop();
        save(ret, basename+ext, rom.mime);
    } catch(e) {
        if (typeof(e)=='string') console.log(e);
        else throw e;
    }
}

function ensureDirectoryExistence(dirname) {
    fs.mkdirSync(dirname, {recursive: true});
}

module.exports.runFiles = runFiles;
module.exports.setConfig = setConfig;


