const {runFiles, setConfig} = require("./src/Patcher");
const {version} =  require('./package.json');

const {app, BrowserWindow, ipcMain, dialog} = require("electron");

app.whenReady().then(() => {
    const mainWindow = new BrowserWindow({
        width: 650,
        height: 700,
        resizable: false,
        autoHideMenuBar: true,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true
        },
    });


    ipcMain.on("startPatch", async (evt, data) => {
        setConfig(data.originalRomPath, data.patchPath, data.savePath, evt)
        await runFiles();
    })

    ipcMain.on("version", async (evt) => {
        mainWindow.send("version", version);
    })

    ipcMain.on("dialog", async(evt, channel, defaultPath="./", isFile=false) => {
        mainWindow.send("debug", channel, defaultPath, isFile)
        var path = await dialog.showOpenDialog({
            properties: isFile ?  ["openFile"] : ["openDirectory"],
            defaultPath: defaultPath || "./"
        });
        mainWindow.send("debug", path)
        if (path.filePaths[0]) {
            mainWindow.send("dialogFinish", channel, path.filePaths[0]);
        }
    })



    mainWindow.loadFile("./public/index.html");

});


app.on("window-all-closed", () => {
    app.quit();
});
